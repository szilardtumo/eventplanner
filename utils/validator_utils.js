const db = require('../db/db');

function eventExists(id) {
  return db.Event.findByPk(id)
    .then((event) => {
      if (event === null) {
        throw Error('Nem létezik rendezvény ezzel az ID-val');
      }

      return true;
    });
}

async function userIdExists(userId, eventId, action) {
  const userExists = await db.User.findByPk(userId, {
    include: [{
      model: db.Event,
      where: {
        id: eventId,
      },
    }],
  })
    .then((user) => user !== null);

  if (action === 'join' && userExists) {
    throw new Error('Már csatlakozott a rendezvényhez');
  } else if (['quit', 'upload'].includes(action) && !userExists) {
    throw new Error('Még nem szervezője a rendezvénynek');
  }

  return true;
}

function isValidEndingDate(startingDate, endingDate) {
  if (new Date(endingDate) < new Date(startingDate)) {
    throw new Error('A befejezési dátum formátuma nem megfelelő, vagy nem a kezdési dátum után van');
  }
  return true;
}

function isValidImage(image) {
  if (!image.type.startsWith('image/')) {
    throw new Error('A megadott file nem kép formátumú');
  }
  if (image.type.size > 15728640) {
    throw new Error('A file mérete túl nagy. Maximális megengedett méret 15MB');
  }

  return true;
}

function usernameExists(username) {
  return db.User.findOne({ where: { username } })
    .then((user) => user !== null);
}

function logErrors(errors) {
  errors.array().forEach((error) => {
    console.log(`Validation error at ${error.param}: ${error.value} (${error.msg})`);
  });
}

module.exports = {
  eventExists,
  usernameExists,
  userIdExists,
  isValidEndingDate,
  isValidImage,
  logErrors,
};
