const fs = require('fs');
const express = require('express');
const handlebars = require('express-handlebars');
const helpers = require('handlebars-helpers');
const session = require('express-session');
const flash = require('connect-flash');
const router = require('./routes/router');
const api = require('./routes/api');
const errorMiddleware = require('./middleware/error');
const handlebarsSessionMiddleware = require('./middleware/handlebars_session');
const config = require('./config');

const app = express();
app.set('view engine', 'hbs');
app.set('views', config.handlebars.views);
app.engine('hbs', handlebars({
  ...config.handlebars,
  helpers: helpers(['comparison']),
}));

if (!fs.existsSync(config.app.uploadDir)) {
  fs.mkdirSync(config.app.uploadDir);
}

app.use(express.static(config.app.staticDir));
app.use(express.static(config.app.uploadDir));

app.use(express.urlencoded({ extended: true }));
app.use(session({
  secret: config.session.secret,
  resave: false,
  saveUninitialized: true,
}));
app.use(handlebarsSessionMiddleware);
app.use(flash());

app.use(router);
app.use('/api', api);

app.use(errorMiddleware);

app.listen(config.app.port, () => console.log(`Server listening on http://localhost:${config.app.port}...`));
