const Sequelize = require('sequelize');
const config = require('../config');
const EventModel = require('../models/event');
const UserModel = require('../models/user');
const ImageModel = require('../models/image');

const sequelize = new Sequelize(config.db.url, {
  pool: config.db.pool,
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
});

const Event = EventModel(sequelize, Sequelize);
const User = UserModel(sequelize, Sequelize);
const Image = ImageModel(sequelize, Sequelize);

Event.belongsToMany(User, { through: 'event_user' });
User.belongsToMany(Event, { through: 'event_user' });
Event.hasMany(Image, { onDelete: 'cascade', hooks: true, foreignKeyConstraint: true });
Image.belongsTo(Event, { onDelete: 'cascade', hooks: true, foreignKeyConstraint: true });
User.hasMany(Image);
Image.belongsTo(User);

sequelize.sync()
  .then(() => {
    console.log('Synced database and tables!');

    // insert dummy data if there isn't any in the database to provide easier testing
    User.findByPk(1).then((user) => {
      if (!user) {
        User.create({
          username: 'admin',
          fullName: 'Adminisztrátor',
          password: '51602a271c62109b528e1c966441ddbb415632f56ec1111099a5ab509edd3c12edbdc07118fc1966112ad338a67a96f7',
          role: 'admin',
        });
        Event.create({
          name: 'Foci világbajnokság',
          startingDate: '2022-06-21',
          endingDate: '2022-08-10',
          location: 'Focipálya',
        });
      }
    });
  });

module.exports = {
  Event,
  User,
  Image,
};
