const {
  check, param, body, buildCheckFunction,
} = require('express-validator');
const db = require('../db/db');
const utils = require('../utils/validator_utils');

const checkFieldsAndFiles = buildCheckFunction(['fields', 'files']);
const validators = {};

validators.register = [
  check('username').custom(async (name) => {
    const exists = await utils.usernameExists(name);
    if (exists) {
      throw Error('Már létezik ez a felhasználónév!');
    }
    return true;
  }),
  check('fullName').notEmpty().withMessage('A teljes név megadása kötelező'),
  check('password').isLength({ min: 6 }).withMessage('A jelszónak minimum 6 karakter hosszúnak kell lennie!'),
  check('passwordConfirm')
    .custom((passwordConfirm, { req }) => passwordConfirm === req.body.password)
    .withMessage('A jelszavak nem egyeznek meg!'),
];

validators.login = [
  check('username').custom(async (name) => {
    const exists = await utils.usernameExists(name);
    if (!exists) {
      throw Error('A felhasználó nem létezik!');
    }
    return true;
  }),
];

validators.showEvent = [
  param('id').custom(utils.eventExists).toInt(),
];

validators.joinEvent = [
  body('eventId').custom(utils.eventExists).toInt(),
  body('action').isIn(['join', 'quit']).withMessage('Nem megengedett művelet'),
];

validators.createEvent = [
  check('name').notEmpty().withMessage('A rendezvény neve nincs megadva'),
  check('startingDate').isISO8601().withMessage('A kezdési dátum formátuma nem megfelelő'),
  check('endingDate').isISO8601()
    .custom((endingDate, { req }) => {
      const { startingDate } = req.body;
      return utils.isValidEndingDate(startingDate, endingDate);
    }),
  check('location').notEmpty().withMessage('A helyszín nincs megadva'),
];

validators.uploadImage = [
  checkFieldsAndFiles('eventId').custom(utils.eventExists).toInt(),
  checkFieldsAndFiles('userId').custom(async (userId, { req }) => {
    const eventId = await db.Event.findByPk(req.fields.eventId)
      .then((event) => event.get().id);

    return utils.userIdExists(userId, eventId, 'upload');
  }).toInt(),
  checkFieldsAndFiles('image').custom((image) => utils.isValidImage(image)),
];

validators.deleteEvent = [
  check('id')
    .custom(utils.eventExists)
    .toInt(),
];

validators.updateEvent = [
  check('id').custom(utils.eventExists).toInt(),
];

function validate(form) {
  return validators[form] || [];
}

module.exports = validate;
