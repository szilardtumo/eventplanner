module.exports = (req, res) => {
  res.status(404).render('error', { message: 'A kért oldal nem található!' });
};
